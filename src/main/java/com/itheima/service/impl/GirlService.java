package com.itheima.service.impl;

import com.itheima.entity.GirlEntity;
import com.itheima.mapper.GirlMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class GirlService {
    @Autowired
    private GirlMapper girlMapper;

    public void insertTwo(){
        GirlEntity girlA = new GirlEntity();
        girlA.setCupSize("A");
        girlA.setAge(18);
        girlMapper.insert(girlA);

        GirlEntity girlB = new GirlEntity();
        girlB.setCupSize("B");
        girlB.setAge(19);
        girlMapper.insert(girlB);
    }
}
