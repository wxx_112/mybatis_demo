package com.itheima.utils;

/**
 * 统一返回码
 */
public enum ResultCode {

    SUCCESS("0","成功"),
    ERROR("-1","系统异常"),
    PARAM_ERROR("1001","参数异常"),
    USER_EXIST_ERROR("2001","用户已经存在"),
    USER_ACCOUNT_ERROR("2002","账号或密码错误"),
    USER_NOT_EXIST("2003","用户名不存在"),
    USER_PASSWORD_ERROR("2004","用户或密码不能为空");
    public String code;
    public String msg;

    ResultCode(String code, String msg) {
        this.code = code;
        this.msg = msg;
    }
}
