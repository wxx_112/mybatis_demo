package com.itheima.utils;
import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;

import javax.imageio.IIOException;
import java.io.IOException;
import java.io.InputStream;


/**
 * 封装连接mysql数据源的方法，用于单元测试
 */
public class SqlSessionUtils {

    public static SqlSession getSqlSession() throws IOException {
        SqlSession sqlSession = null;
        try {
            InputStream is = Resources.getResourceAsStream("mybatis-config.xml");
            SqlSessionFactory sqlSessionFactory = new SqlSessionFactoryBuilder().build(is);
            sqlSession = sqlSessionFactory.openSession(true);
        }catch (IIOException e){
            e.printStackTrace();
        }
        return sqlSession;
            // 5、释放资源
//            sqlSession.close();
    }

}
