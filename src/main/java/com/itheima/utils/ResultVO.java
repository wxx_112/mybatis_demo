package com.itheima.utils;

/**
 * 响应类模板
 */

public class ResultVO {
    /*  状态码  */
    private String code;

    /*  状态码描述  */
    private String message;

    /*  返回数据  */
    private Object data;

    /**
     * 创建一个无数据的成功，一般用于增删改查
     * @return
     */
    public static ResultVO success(){
        ResultVO resultVO = new ResultVO();
        resultVO.code = ResultCode.SUCCESS.code;
        resultVO.message = ResultCode.SUCCESS.msg;
        return resultVO;
    }

    /**
     * 返回有数据的成功，一般用于查询
     * @param data
     * @return
     */
    public static ResultVO success(Object data){
        ResultVO resultVO = new ResultVO();
        resultVO.code = ResultCode.SUCCESS.code;
        resultVO.message = ResultCode.SUCCESS.msg;
        resultVO.data = data;
        return resultVO;
    }


    /**
     * 有参失败
     * @param code
     * @param message
     * @return
     */
    public static ResultVO error(String code, String message){
        ResultVO resultVO = new ResultVO();
        resultVO.code = code;
        resultVO.message = message;
        return resultVO;
    }

    public ResultVO() {
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Object getData() {
        return data;
    }

    public void setData(Object data) {
        this.data = data;
    }

}
