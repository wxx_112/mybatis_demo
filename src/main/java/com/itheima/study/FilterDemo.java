package com.itheima.study;

import java.util.Comparator;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class FilterDemo {

    public static void main(String[] args) {
        // 获取Stream对象
        Stream<String> words = Stream.of("Hogwarts", "霍格沃兹");

//        // count()：统计流对象中的数量
//        long n = words.count();
//        System.out.println(n);


//        // forEach()：遍历
//        words.forEach(e -> System.out.println(e+"111"));


//        // 返回list数组
//        List<String> result = words
//                .filter(s -> s.startsWith("H"))
//                .collect(Collectors.toList());
//        System.out.println(result);

//        //返回set集合
//        Set<String> mySet = words
//                .filter(s -> s.startsWith("H"))
//                .collect(Collectors.toSet());
//        System.out.println(mySet);

//        // limit()：保留前n个元素
//        List<String> result = words
//                .limit(3)
//                .collect(Collectors.toList());
//        System.out.println(result);

        Stream<Integer> num = Stream.of(120, 100);
        // max()：获取最大值
        Integer ret = num
                .max(Comparator.comparing(e -> e)).orElse(null);
        System.out.println(ret);

//        // map():映射
//        List<String> result = words
//                .map(x -> x + "666")
//                .collect(Collectors.toList());
//        System.out.println(result);


    }
}
