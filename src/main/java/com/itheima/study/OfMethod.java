package com.itheima.study;


import java.util.stream.Stream;

/**
 *
 */
public class OfMethod {

    public static void main(String[] args) {
        // of() 方法
        Stream<Integer> num = Stream.of(1, 2, 3);

        // of方法 接收数组
        String[] words = {"Java","Python","Go"};
        Stream<String> wordsStream = Stream.of(words);

        System.out.println(num);
        System.out.println(wordsStream);
    }
}
