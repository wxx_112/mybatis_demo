package com.itheima.study;

import java.time.LocalDateTime;

public class LocalDateTimeDemo {

    public static void main(String[] args) {
        // now()：返回当前系统时间的对象
        LocalDateTime tmNow = LocalDateTime.now();
        System.out.println("now():" + tmNow);

        // of()：自定义日期时间对象
        LocalDateTime tmOf = LocalDateTime.of(2023, 6, 18, 20, 30, 20);
        System.out.println("of():" + tmOf);

    }
}
