package com.itheima.study;

import java.util.*;
import java.util.stream.Stream;


/**
 * 通过stream方法获取 数据流对象
 */
public class SteamMethod {

    public static void main(String[] args) {
        // Collection 接口
        Stream<Integer> listS = new ArrayList<Integer>().stream();
        Stream<Integer> setS = new HashSet<Integer>().stream();
        Stream<Integer> vectorS = new Vector<Integer>().stream();

        System.out.println("数组:"+listS);

        // Map 接口
        HashMap<String, String> map = new HashMap<>();

        // key
        Stream<String> keys = map.keySet().stream();
        // value
        Stream<String> values = map.values().stream();
        // k-v
        Stream<Map.Entry<String, String>> entryStream = map.entrySet().stream();

    }
}
