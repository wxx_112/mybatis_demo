package com.itheima.study;

import org.springframework.format.annotation.DateTimeFormat;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class FormatterDemo {

    public static void main(String[] args) {
        // 实例化对象
        String fmt = "yyyy-MM-dd hh:mm:ss";
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern(fmt);

        // 将日期时间对象——>字符串
        String timeStr = formatter.format(LocalDateTime.now());
        System.out.println(timeStr);
    }
}
