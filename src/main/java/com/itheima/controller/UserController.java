package com.itheima.controller;
import com.itheima.entity.User;
import com.itheima.pojo.UserRequest;
import com.itheima.service.impl.UserServiceImpl;
import com.itheima.utils.ResultVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

@Controller
@RestController
public class UserController {

//    @Autowired
//    private UserService userService;

    @Autowired
    private UserServiceImpl userServiceiml;

    // 1、全表查询
    @RequestMapping(value = "/selectAll",method = RequestMethod.POST)
    public ResultVO selectAll(@RequestBody UserRequest user){
        return ResultVO.success(userServiceiml.selectAll());
    }

    // 2、通过id查询
    @RequestMapping(value = "/selectid",method = RequestMethod.POST)
    public ResultVO selectid(@RequestBody UserRequest user){

        return ResultVO.success(userServiceiml.selectid(user));
    }

    // 3、通过id批量查询
    @RequestMapping(value = "/selectlist",method = RequestMethod.POST)
    public ResultVO selectlist(@RequestBody UserRequest userRe){
        return ResultVO.success(userServiceiml.selectlist(userRe));
    }

    // 4、注册接口
    @RequestMapping(value = "/register",method = RequestMethod.POST)
    public ResultVO register(@RequestBody User user){
        return userServiceiml.register(user);
    }

    // 5、登录
    @RequestMapping(value = "/login",method = RequestMethod.POST)
    public ResultVO login(@RequestBody UserRequest user){
        return userServiceiml.login(user);
    }

    @RequestMapping(value = "/deleteId",method = RequestMethod.POST)
    public ResultVO delete(@RequestBody UserRequest user){
        return userServiceiml.deleteId(user);
    }

//    @RequestMapping(value = "/deletemap",method = RequestMethod.POST)
//    public ResultVO deletemap(@RequestBody User user){
//        return userService.deleteMap(user);
//    }

    @RequestMapping(value = "/deletelist",method = RequestMethod.POST)
    public ResultVO deletelist(@RequestBody UserRequest user){
        return ResultVO.success(userServiceiml.deletelist(user));
    }

    @RequestMapping(value = "/update",method = RequestMethod.POST)
    public ResultVO update(@RequestBody User user){
//        return userService.update(user);
        return ResultVO.success(userServiceiml.update(user));
    }

}
