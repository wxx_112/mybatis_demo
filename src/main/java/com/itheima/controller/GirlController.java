package com.itheima.controller;

import com.itheima.entity.GirlEntity;
import com.itheima.mapper.GirlMapper;
import com.itheima.service.impl.GirlService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class GirlController {

    @Autowired
    private GirlMapper girlMapper;

    @Autowired
    private GirlService girlService;

    /**
     * 查看所有女生列表
     * @return
     */
    @GetMapping(value = "/girls/selectAll")
    public List<GirlEntity> girlList()
    {
        return girlMapper.findAll();
    }


    /**
     * 自测调试
     * @return
     */
    @GetMapping("/test")
    public String test(){
        return "欢迎来到SpringBoot奇妙世界！helloworld";
    }
    // 这个代码是成功构建了，

    /**
     * 自测调试
     * @return
     */
    @GetMapping("/wang")
    public String wang(){
        return "欢迎来到SpringBoo---wangxingxing！";
    }
    // 这个代码是我后面加的，通过git上传后就无法构建了哈？什么意思啊？稍等我理一理

    /**
     * 添加一个女生
     * @param cupSize
     * @param age
     * @return
     */
    @PostMapping(value = "/girls/add")
    public Integer girlAdd(@RequestParam("cupSize") String cupSize,
                              @RequestParam("age") Integer age){
        GirlEntity girl = new GirlEntity();
        girl.setCupSize(cupSize);
        girl.setAge(age);

        return girlMapper.insert(girl);
    }

    /**
     * 查询一个女生
     * @param id
     * @return
     */
    @GetMapping(value = "/girls/select/{id}")
    public GirlEntity girlFindOne(@PathVariable("id") Integer id) {

        return girlMapper.findById(id);
    }

    /**
     * 更新
     * @param id
     * @param cupSize
     * @param age
     * @return
     */
    @PutMapping(value = "/girls/update/{id}")
    public Integer girlUpdate(@PathVariable("id") Integer id,
                                 @RequestParam("cupSize") String cupSize,
                                 @RequestParam("age") Integer age) {
        GirlEntity gir = new GirlEntity();
        gir.setId(id);
        gir.setCupSize(cupSize);
        gir.setAge(age);
        return girlMapper.insert(gir);
    }

    // 删除
    @DeleteMapping(value = "/girls/delete/{id}")
    public void girlDelete(@PathVariable("id") Integer id)
    {
        girlMapper.deleteById(id);
    }

    // 通过年龄查询女生列表
    @GetMapping(value = "/girls/age/{age}")
    public List<GirlEntity> girlListByAge(@PathVariable("age") Integer age){
        return girlMapper.findByAge(age);
    }

    @PostMapping(value = "/girls/two")
    public void girlTwo() {

        girlService.insertTwo();

    }
}
