package com.itheima.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.itheima.entity.User;
import org.apache.ibatis.annotations.Mapper;



import java.util.List;


@Mapper
//@Repository
public interface UserMapper extends BaseMapper<User> {

    List<User> selectAll();     // 查询所有用户

    User queryByUsername(String username);    //用户登录时，验证用户

    User selectById(Integer id);



//    List<User> selectBatchIds(@Param("ids") List<String> ids);


    /*
        MyBatis 参数封装：
            * 单个参数：
                1、POJO类型：直接使用，属性名 和 参数占位符 一致
                2、Map集合：直接使用，键名 和参数占位符 一致
                3、Collection：封装为Map集合，可以使用@Param注解，替换Map集合中默认的arg键名
                    map.put("arg0",collection集合);
                    map.put("collection",collection集合);
                4、List：封装为Map集合，可以使用@Param注解，替换Map集合中默认的arg键名
                    map.put("arg0",list集合);
                    map.put("collection",list集合);
                    map.put("list",list集合);
                5、Array：封装为Map集合，可以使用@Param注解，替换Map集合中默认的arg键名
                    map.put("arg0",数组);
                    map.put("array",数组);
                6、其他类型：直接使用
            * 多个参数：封装为Map集合，可以使用@Param注解，替换Map集合中默认的arg键名
                map.put("arg0",参数值1)
                map.put("param1",参数值1)
                map.put("param2",参数值2)
                map.put("arg1",参数值2)
                -------------------@Param("username")替换成map.put("username",参数值1)，所以xm文件中可以用username，不然只能用arg键
                map.put("username",参数值1)
                map.put("param1",参数值1)
                map.put("param2",参数值2)
                map.put("arg1",参数值2)
     */
//    User select(@Param("username") String username, @Param("password") String password);

}


