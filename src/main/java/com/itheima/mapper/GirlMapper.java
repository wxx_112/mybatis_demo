package com.itheima.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.itheima.entity.GirlEntity;
import org.apache.ibatis.annotations.Mapper;


import java.util.List;

@Mapper
public interface GirlMapper extends BaseMapper<GirlEntity> {

    // 通过年龄来查询
    List<GirlEntity> findByAge(Integer age);

    List<GirlEntity> findAll();

    GirlEntity findById(Integer id);

}
