package com.itheima.mapper;

import com.itheima.entity.Area;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;


//@Mapper
public interface AreaDaoMapper {

    List<Area> queryArea();

    Area queryAreaById(int areaId);

//    int insertAArea(Area area);
//
//    int updateArea(Area area);
//
//    int deleteArea(int areaId);


}

