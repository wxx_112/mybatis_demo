package com.itheima.mapper;


import com.itheima.entity.Brand;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

//@Mapper
public interface BrandMapper {

    /**
     * 查询所有
     */
    List<Brand> selectAll();

    /**
     * 查看详情
     */
    Brand selectById(int id);  // Shift+Alt+Enter快捷键生成xml中

    /**
     * 多条件查询
     * * 参数接收
     *      1、散装参数：如果方法中有多个参数，需要使用@Param("SQL参数占位符名称")
     *      2、
     */
    List<Brand> selectByCondition(@Param("status") int status,@Param("companyName") String companyName,@Param("brandName") String brandName);

    List<Brand> selectByCondition(Brand brand);

    List<Brand> selectByCondition(Map map);

    /**
     * 单条件动态查询
     * @param brand
     * @return
     */
    List<Brand> selectByConditionSingle(Brand brand);

    /**
     * 添加无返回
     */
    void add(Brand brand);

    /**
     * 添加有返回关键字
     */
    void add_id(Brand brand);

    /**
     * 更新全部字段
     */
    int update(Brand brand);

    /**
     * 动态更新个别字段
     */
    int update_data(Brand brand);

    /**
     * 根据id删除一个
     */
    void delete_one(int id);

    /**
     * 根据id批量删除
     */
    void delete_all(int[] ids);

}


