package com.itheima.test;
import com.itheima.entity.User;
import com.itheima.mapper.UserMapper;
import com.itheima.utils.SqlSessionUtils;
import org.apache.ibatis.session.SqlSession;
import org.junit.Test;

import java.io.IOException;
import java.util.List;

public class MyTest {

    @Test
    public void testGetAllEmp() throws IOException {
        SqlSession sqlSession = SqlSessionUtils.getSqlSession();
        UserMapper mapper = sqlSession.getMapper(UserMapper.class);
        List<User> list = mapper.selectAll();
        list.forEach(System.out::println);
        // 释放资源，也可以不释放
        sqlSession.close();

    }

}
