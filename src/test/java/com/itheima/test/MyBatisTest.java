package com.itheima.test;

import com.itheima.mapper.BrandMapper;
import com.itheima.entity.Brand;
import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;
import org.junit.Test;

import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class MyBatisTest {
    @Test
    public void testSelectAll() throws IOException {
        // 1、获取SqlSessionFactory
        String resource = "mybatis-config.xml";
        InputStream inputStream = Resources.getResourceAsStream(resource);
        SqlSessionFactory sqlSessionFactory = new SqlSessionFactoryBuilder().build(inputStream);

        // 2、获取SqlSession对象
        SqlSession sqlSession = sqlSessionFactory.openSession();

        // 3、获取Mapper接口的代理对象
        BrandMapper brandMapper = sqlSession.getMapper(BrandMapper.class);

        // 4、执行方法
        List<Brand> brands = brandMapper.selectAll();
        System.out.println(brands);

        // 5、释放资源
        sqlSession.close();
    }

    @Test
    public void testSelectById() throws IOException {
        // 接收参数
        int id = 2;

        // 1、获取SqlSessionFactory
        String resource = "mybatis-config.xml";
        InputStream inputStream = Resources.getResourceAsStream(resource);
        SqlSessionFactory sqlSessionFactory = new SqlSessionFactoryBuilder().build(inputStream);

        // 2、获取SqlSession对象
        SqlSession sqlSession = sqlSessionFactory.openSession();

        // 3、获取Mapper接口的代理对象
        BrandMapper brandMapper = sqlSession.getMapper(BrandMapper.class);

        // 4、执行方法
        Brand brand = brandMapper.selectById(id);
        System.out.println(brand);

        // 5、释放资源
        sqlSession.close();
    }

    // 多参数查询  ——  散装参数：如果方法中有多个参数，需要使用@Param(“SQL参数占位符名称”)
    @Test
    public void testSelectByCondition() throws IOException {
        // 接收参数
        int status = 1;
        String companyName = "华为";
        String brandName = "华为";

        // 处理参数
        companyName = "%" + companyName + "%";
        brandName = "%" + brandName + "%";

        // 1、获取SqlSessionFactory
        String resource = "mybatis-config.xml";
        InputStream inputStream = Resources.getResourceAsStream(resource);
        SqlSessionFactory sqlSessionFactory = new SqlSessionFactoryBuilder().build(inputStream);

        // 2、获取SqlSession对象
        SqlSession sqlSession = sqlSessionFactory.openSession();

        // 3、获取Mapper接口的代理对象
        BrandMapper brandMapper = sqlSession.getMapper(BrandMapper.class);

        // 4、执行方法
        List<Brand> brands = brandMapper.selectByCondition(status, companyName, brandName);
        System.out.println(brands);

        // 5、释放资源
        sqlSession.close();
    }

    // 多参数查询  ——  对象参数：对象的属性名称要和参数占位符名称一致
    @Test
    public void testSelectByCondition_Brand() throws IOException {
        // 接收参数
        int status = 1;
        String companyName = "华为";
        String brandName = "华为";

        // 处理参数
        companyName = "%" + companyName + "%";
        brandName = "%" + brandName + "%";

        // 封装对象
        Brand brand = new Brand();
        brand.setStatus(status);
        brand.setCompanyName(companyName);
        brand.setBrandName(brandName);

        // 1、获取SqlSessionFactory
        String resource = "mybatis-config.xml";
        InputStream inputStream = Resources.getResourceAsStream(resource);
        SqlSessionFactory sqlSessionFactory = new SqlSessionFactoryBuilder().build(inputStream);

        // 2、获取SqlSession对象
        SqlSession sqlSession = sqlSessionFactory.openSession();

        // 3、获取Mapper接口的代理对象
        BrandMapper brandMapper = sqlSession.getMapper(BrandMapper.class);

        // 4、执行方法
        List<Brand> brands = brandMapper.selectByCondition(brand);
        System.out.println(brands);

        // 5、释放资源
        sqlSession.close();
    }


    // 多参数查询  ——  map集合：集合对象的属性名称要和参数占位符名称一致
    @Test
    public void testSelectByCondition_map() throws IOException {
        // 接收参数
        int status = 1;
        String companyName = "华为";
        String brandName = "华为";

        // 处理参数
        companyName = "%" + companyName + "%";
        brandName = "%" + brandName + "%";


        Map map = new HashMap();
        map.put("status",status);
        map.put("companyName",companyName);
        map.put("brandName",brandName);

        // 1、获取SqlSessionFactory
        String resource = "mybatis-config.xml";
        InputStream inputStream = Resources.getResourceAsStream(resource);
        SqlSessionFactory sqlSessionFactory = new SqlSessionFactoryBuilder().build(inputStream);

        // 2、获取SqlSession对象
        SqlSession sqlSession = sqlSessionFactory.openSession();

        // 3、获取Mapper接口的代理对象
        BrandMapper brandMapper = sqlSession.getMapper(BrandMapper.class);

        // 4、执行方法
        List<Brand> brands = brandMapper.selectByCondition(map);
        System.out.println(brands);

        // 5、释放资源
        sqlSession.close();
    }

    // 单条件查询
    @Test
    public void testSelectByConditionSingle() throws IOException {
        // 接收参数
        int status = 1;
        String companyName = "华为";
        String brandName = "华为";

        // 处理参数
        companyName = "%" + companyName + "%";
        brandName = "%" + brandName + "%";

        // 封装对象
        Brand brand = new Brand();
//        brand.setStatus(status);
//        brand.setCompanyName(companyName);
//        brand.setBrandName(brandName);

        // 1、获取SqlSessionFactory
        String resource = "mybatis-config.xml";
        InputStream inputStream = Resources.getResourceAsStream(resource);
        SqlSessionFactory sqlSessionFactory = new SqlSessionFactoryBuilder().build(inputStream);

        // 2、获取SqlSession对象
        SqlSession sqlSession = sqlSessionFactory.openSession();

        // 3、获取Mapper接口的代理对象
        BrandMapper brandMapper = sqlSession.getMapper(BrandMapper.class);

        // 4、执行方法
        List<Brand> brands = brandMapper.selectByConditionSingle(brand);

        System.out.println(brands);

        // 5、释放资源
        sqlSession.close();
    }

    // 添加数据
    @Test
    public void testAdd() throws IOException {
        // 接收参数
        int status = 1;
        String companyName = "波导手机";
        String brandName = "波导";
        String description = "手机中的战斗机";
        int ordered = 100;

        // 封装对象
        Brand brand = new Brand();
        brand.setStatus(status);
        brand.setCompanyName(companyName);
        brand.setBrandName(brandName);
        brand.setDescription(description);
        brand.setOrdered(ordered);


        // 1、获取SqlSessionFactory
        String resource = "mybatis-config.xml";
        InputStream inputStream = Resources.getResourceAsStream(resource);
        SqlSessionFactory sqlSessionFactory = new SqlSessionFactoryBuilder().build(inputStream);

        // 2、获取SqlSession对象
        SqlSession sqlSession = sqlSessionFactory.openSession(true);

        // 3、获取Mapper接口的代理对象
        BrandMapper brandMapper = sqlSession.getMapper(BrandMapper.class);

        // 4、执行方法
        brandMapper.add(brand);
//        sqlSession.commit();  // SqlSession sqlSession = sqlSessionFactory.openSession(true);中传true，则不需要手机提交事务

        // 5、释放资源
        sqlSession.close();
    }

    // 添加数据，并返回主键id
    @Test
    public void testAdd_id() throws IOException {
        // 接收参数
        int status = 1;
        String companyName = "波导手机";
        String brandName = "波导";
        String description = "手机中的战斗机";
        int ordered = 100;

        // 封装对象
        Brand brand = new Brand();
        brand.setStatus(status);
        brand.setCompanyName(companyName);
        brand.setBrandName(brandName);
        brand.setDescription(description);
        brand.setOrdered(ordered);


        // 1、获取SqlSessionFactory
        String resource = "mybatis-config.xml";
        InputStream inputStream = Resources.getResourceAsStream(resource);
        SqlSessionFactory sqlSessionFactory = new SqlSessionFactoryBuilder().build(inputStream);

        // 2、获取SqlSession对象
        SqlSession sqlSession = sqlSessionFactory.openSession(true);

        // 3、获取Mapper接口的代理对象
        BrandMapper brandMapper = sqlSession.getMapper(BrandMapper.class);

        // 4、执行方法
        brandMapper.add_id(brand);
        Integer id = brand.getId();
        System.out.println(id);

        // 5、释放资源
        sqlSession.close();
    }

    // 修改-修改全部字段
    @Test
    public void testUpdate() throws IOException {
        // 接收参数
        int status = 1;
        String companyName = "小米手机";
        String brandName = "小米";
        String description = "小米手机，手机中的战斗机";
        int ordered = 200;
        int id = 5;

        // 封装对象
        Brand brand = new Brand();
        brand.setStatus(status);
        brand.setCompanyName(companyName);
        brand.setBrandName(brandName);
        brand.setDescription(description);
        brand.setOrdered(ordered);
        brand.setId(id);

        // 1、获取SqlSessionFactory
        String resource = "mybatis-config.xml";
        InputStream inputStream = Resources.getResourceAsStream(resource);
        SqlSessionFactory sqlSessionFactory = new SqlSessionFactoryBuilder().build(inputStream);

        // 2、获取SqlSession对象
        SqlSession sqlSession = sqlSessionFactory.openSession(true);

        // 3、获取Mapper接口的代理对象
        BrandMapper brandMapper = sqlSession.getMapper(BrandMapper.class);

        // 4、执行方法
        int count = brandMapper.update(brand);
        System.out.println(count);
        System.out.println(brand);

        // 5、释放资源
        sqlSession.close();
    }


    // 修改-修改动态字段
    @Test
    public void testUpdate_data() throws IOException {
        // 接收参数
        int status = 0;
        String companyName = "小米手机";
        String brandName = "小米";
        String description = "小米手机，手机中的战斗机";
        int ordered = 200;
        int id = 5;

        // 封装对象
        Brand brand = new Brand();
        brand.setStatus(status);
//        brand.setCompanyName(companyName);
//        brand.setBrandName(brandName);
//        brand.setDescription(description);
//        brand.setOrdered(ordered);
        brand.setId(id);

        // 1、获取SqlSessionFactory
        String resource = "mybatis-config.xml";
        InputStream inputStream = Resources.getResourceAsStream(resource);
        SqlSessionFactory sqlSessionFactory = new SqlSessionFactoryBuilder().build(inputStream);

        // 2、获取SqlSession对象
        SqlSession sqlSession = sqlSessionFactory.openSession(true);

        // 3、获取Mapper接口的代理对象
        BrandMapper brandMapper = sqlSession.getMapper(BrandMapper.class);

        // 4、执行方法
        int count = brandMapper.update_data(brand);
        System.out.println(count);
        System.out.println(brand);

        // 5、释放资源
        sqlSession.close();
    }


    // 删除一个数据
    @Test
    public void testdelete_one() throws IOException {
        // 接收参数
        int id = 5;

        // 1、获取SqlSessionFactory
        String resource = "mybatis-config.xml";
        InputStream inputStream = Resources.getResourceAsStream(resource);
        SqlSessionFactory sqlSessionFactory = new SqlSessionFactoryBuilder().build(inputStream);

        // 2、获取SqlSession对象
        SqlSession sqlSession = sqlSessionFactory.openSession(true);

        // 3、获取Mapper接口的代理对象
        BrandMapper brandMapper = sqlSession.getMapper(BrandMapper.class);

        // 4、执行方法
        brandMapper.delete_one(id);
        System.out.println(id);

        // 5、释放资源
        sqlSession.close();
    }

    // 批量删除
    @Test
    public void testdelete_all() throws IOException {
        // 接收参数
        int[] ids = {2};

        // 1、获取SqlSessionFactory
        String resource = "mybatis-config.xml";
        InputStream inputStream = Resources.getResourceAsStream(resource);
        SqlSessionFactory sqlSessionFactory = new SqlSessionFactoryBuilder().build(inputStream);

        // 2、获取SqlSession对象
        SqlSession sqlSession = sqlSessionFactory.openSession(true);

        // 3、获取Mapper接口的代理对象
        BrandMapper brandMapper = sqlSession.getMapper(BrandMapper.class);

        // 4、执行方法
        brandMapper.delete_all(ids);

        // 5、释放资源
        sqlSession.close();
    }


}
