package com.itheima.test;

import com.itheima.entity.User;
import com.itheima.mapper.UserMapper;
import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;
import org.junit.Test;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;

public class UserMapperTest {

    @Test
    public void testSelect() throws IOException {
//        // 接收参数
//        int[] ids = {2};

        // 1、获取SqlSessionFactory
        String resource = "mybatis-config.xml";
        InputStream inputStream = Resources.getResourceAsStream(resource);
        SqlSessionFactory sqlSessionFactory = new SqlSessionFactoryBuilder().build(inputStream);

        // 2、获取SqlSession对象
        SqlSession sqlSession = sqlSessionFactory.openSession(true);

        // 3、获取Mapper接口的代理对象
        UserMapper userMapper = sqlSession.getMapper(UserMapper.class);

//        // 4、执行方法
//        String username = "zhangsan";
//        String password = "123";

        List<User> userList = userMapper.selectAll();

        System.out.println(userList);

        // 5、释放资源
        sqlSession.close();
    }

    @Test
    public void testSelectById() throws IOException {
        // 接收参数
        int id = 2;

        // 1、获取SqlSessionFactory
        String resource = "mybatis-config.xml";
        InputStream inputStream = Resources.getResourceAsStream(resource);
        SqlSessionFactory sqlSessionFactory = new SqlSessionFactoryBuilder().build(inputStream);

        // 2、获取SqlSession对象
        SqlSession sqlSession = sqlSessionFactory.openSession(true);

        // 3、获取Mapper接口的代理对象
        UserMapper userMapper = sqlSession.getMapper(UserMapper.class);

        // 4、执行方法
        User user = userMapper.selectById(id);

        System.out.println(user);

        // 5、释放资源
        sqlSession.close();
    }
}
