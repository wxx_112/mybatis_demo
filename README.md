# mybatis_demo

#### 介绍
用于mybatis练习

黑马mybatis教程全套视频教程，2天Mybatis框架从入门到精通


一、查询user表中所有数据
1、创建user表，添加数据
    create database my_batis;
    use my_batis;
    drop table if exists tb_user;
    create table tb_user(
        id int primary key auto_increment,
        username varchar(20),
        password varchar(20),
        gender char(1),
        addr varchar(30)
    );
    
    insert into tb_user values(1,'zhangsan','123','男','北京');
    insert into tb_user values(2,'李四','234','女','天津');
    insert into tb_user values(3,'王五','11','男','西安');
    
2、创建模块，导入坐标
3、编写MyBatis核心配置文件 --> 替换连接信息，解决硬编码问题
4、编写SQL映射文件 --> 统一管理sql语句，解决硬编码问题
5、编码
    1、定义P0J0类
    2、加载核心配置文件，获取SqlSessionFactory对象
    3、获取SqlSession对象，执行SQL语句
    4、释放资源


二、环境准备
1、创建tb_brand表，添加数据
    drop table if exists tb_brand;
    create table tb_brand
    (
        id int primary key auto_increment,
        brand_name varchar(20),
        company_name varchar(20),
        ordered int,
        description varchar(100),
        status int
    );
    insert into tb_brand (brand_name,company_name,ordered,description,status)
    values ('三只松鼠','三只松鼠股份有限公司',5,'好吃不上火',0),
            ('华为','华为技术有限公司',100,'华为致力于把数字世界带入每个人、每个家庭、每个组织，构建万物互联的智能世界',1),
            ('小米','小米科技有限公司',50,'are you ok',1);
    select * from tb_brand;
2、创建tb_area表，添加数据
    drop table if exists tb_area;
    CREATE TABLE `tb_area` (
      `area_id` INT(2) NOT NULL AUTO_INCREMENT,
      `area_name` VARCHAR(200) NOT NULL,
      `priority` INT(2) NOT NULL DEFAULT '0',
      `create_time` DATETIME DEFAULT NULL,
      `last_edit_time` DATETIME DEFAULT NULL,
      PRIMARY KEY (`area_id`),
      UNIQUE KEY `UK_AREA`(`area_name`)
    ) ENGINE=INNODB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8
#### 软件架构
软件架构说明


#### 安装教程

1.  controller是接口层
2.  entity是响应层
3.  mapper是接口类层
4.  pojo是请求层
5.  service是服务逻辑层

#### 使用说明

1.  xxxx
2.  xxxx
3.  xxxx

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
